const FPS = 1000/64;
var showList = 0;
var aniBar = 0;
var listHeight = 300;
var f;
var c = 0.02;

function main() {
	mainMobileBar();
	let cls=document.getElementsByTagName("li");
	for(let i=0; i<cls.length; i++){
		cls[i].onmouseleave = function() {document.getElementsByClassName("buttonEffect")[i].style.height = "0px";}
		cls[i].onmouseenter = function() {document.getElementsByClassName("buttonEffect")[i].style.height = "3px";}
	}	
	let s = getComp(document.getElementById("container"), "width");
	var e = document.getElementById("map"); 
	if (e) {
		e.style.width = s + "px";
		e.style.height = s + "px";
	}

}
window.onload = function(){
	document.getElementById("button").onclick = function() {
		showList ^= 1;
	}
	setInterval(function(){main()}, FPS);

	var file;
	var b;
	function handleFileSelect(evt) {
		file = evt.target.files[0];
		b = new FileReader();
		b.onload = function(e) {/*
			var r = document.createElement("div")
			var c = e.target.result;
			var t = [];
			r.innerHTML = "t";
			document.body.appendChild(r);*/
			
			var r = document.createElement("div");
			var c = e.target.result;;
			c = c.split(String.fromCharCode(10));
			for (var i in c) {
				var els = 1;
				if (c[i][0] == "@") {c[i] = c[i].replace("@", ""); c[i] = ("</p><hr><h3>").concat(c[i], " zegt:</h3>"); els = 0;}
				if (c[i][0] == "*") {c[i] = c[i].replace("*", ""); c[i] = ("<img src='./images/ster.png' width=15>").repeat(Math.min(parseInt(c[i]), 5)); c[i] += "<br><p style='width: 300px;'>"; els = 0}
				if (els) {c[i] += "<br>";}
			}
			console.log(c);
			r.innerHTML = c.join("");
			console.log(c.join("<br>"));
			document.getElementById("hcenter").appendChild(r);

		}
		b.readAsText(file);
	}

	document.getElementById('files').addEventListener('change', handleFileSelect, false);
}

function mainMobileBar() {
	if (aniBar!=listHeight * showList) {
		aniBar += (6 * (2 - showList) ** 2) * (showList * 2 - 1); 
	}
	if (aniBar > listHeight) {aniBar = listHeight;}
	if (aniBar < 0) {aniBar = 0;}
	f = (aniBar * (c * listHeight + 1))/(c * aniBar + 1);
	if (aniBar != listHeight || aniBar != 0) {document.getElementById("listContainer").style.height = Math.min(f, window.innerHeight) + "px";}
}
function getComp(ele,prop) {
	return parseFloat(window.getComputedStyle(ele).getPropertyValue(prop));
}